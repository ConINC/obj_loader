#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "collection.h"

typedef struct vec2
{
    float x,y;
}VEC2;

typedef struct vec3
{
    float x,y,z;
}VEC3;

typedef struct vec3int_str
{
    int x,y,z;
}VEC3_INT;

typedef struct vec9int_str
{
    int a,b,c, d,e,f, g,h,i;
}VEC9_INT;

typedef struct obj_material
{
    char *colormap_path;
    char *normalmap_path;
    char *specularmap_path;
    char *name;
    float illum;
    float kd1,kd2,kd3;
    float ka1,ka2,ka3;
    float tf1,tf2,tf3;
    float ni;
    float ks1,ks2,ks3;

}OBJ_MATERIAL;

typedef struct obj_face_str
{
    char *material;
    _LIST *triangles;

}OBJ_Face;

typedef struct obj_group_str
{
    char *groupname;
    _LIST *faces;
}OBJ_Group;

typedef struct obj_data_raw
{
    _LIST *points;
    _LIST *uvs;
    _LIST *normals;
    _LIST *gfm_lines;
}Obj_data_raw;

typedef struct obj_model_str
{
    _LIST *groups;
    _LIST *materials;
    _LIST *p,*uv,*n;
}OBJ_MODEL;

/**
 * Opens a filestream and reads the content into a char* buffer
 *
 * @param const char *filename
 * @return char *buffer
 */
char *load_text(const char *filename)
{
    FILE *file;
    long size;
    char *buf;

    file = fopen(filename, "r");
    if(file == NULL)
        return -1;

    fseek(file, 0L, SEEK_END);
    size = ftell(file);

    fseek(file, 0L, SEEK_SET);

    buf = (char*)calloc(size, sizeof(char));

    if(buf == NULL)
        return 1;


    fread(buf, sizeof(char), size, file);
    fclose(file);

    return buf;
}

_LIST* split_buffer(char *buf, char *delimeter)
{
    _LIST *list = list_new();

    char *ptr = strtok(buf, delimeter);

    list_add(list, ptr);

    while(ptr != NULL)
    {
        ptr = strtok(NULL, delimeter);
        list_add(list, ptr);
    }

    return list;
}

Obj_data_raw *extract_points(_LIST* list)
{
    printf("Extracting points\n");
    int i;
    Obj_data_raw *data = malloc(sizeof(Obj_data_raw));
    data->points = list_new();
    data->normals= list_new();
    data->uvs = list_new();
    data->gfm_lines = list_new();

    for(i = 0; i < list_count(list); i++)
    {
        if( strstr(list_at(list, i), "v ") )
        {
            char *ptr = strtok(list_at(list, i)," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            char *ptr4 = strtok(NULL, " ");
            VEC3 *v = malloc(sizeof(VEC3));
            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            v->z = (float)atof(ptr4);
            list_add(data->points, v);

            //printf("V: %f %f %f\n", v->x,v->y,v->z);
        }
        if( strstr(list_at(list, i), "vt ") )
        {
            char *c =  malloc(strlen(list_at(list, i)));
            strcpy(c, list_at(list, i));
            char *ptr = strtok(c," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            VEC2 *v = malloc(sizeof(VEC2));
            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            list_add(data->uvs, v);
            //printf("VT: %f %f\n", v->x,v->y);
        }

        if(strstr(list_at(list, i), "vn ") )
        {
            char *ptr = strtok(list_at(list, i)," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            char *ptr4 = strtok(NULL, " ");
            VEC3 *v = malloc(sizeof(VEC3));

            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            v->z = (float)atof(ptr4);
            list_add(data->normals, v);
            //printf("VN: %f %f %f\n", v->x,v->y,v->z);
        }

        if(strstr(list_at(list, i), "usemtl ") || strstr(list_at(list, i), "mtllib ") || strstr(list_at(list, i), "g ") || strstr(list_at(list, i), "f "))
        {
            list_add(data->gfm_lines, list_at(list, i));
            //printf("GMF LINE: %s\n", list_at(list, i));
        }
    }

    return data;
}

_LIST* create_group_string(Obj_data_raw* dat)
{
    //example of the .obj file
    //g group1
    //usemtl mat1
    //f 1/1/1 2/2/2 4/19/3 3/20/4
    //
    //g group2
    //usemtl mat2
    //f 1/1/1 2/2/2 4/19/3 3/20/4

    //This list contains Lists itself, an Array of Lines for each group!
    _LIST *list_to_grouplists = list_new();
    _LIST *temp_list = NULL;
    int i, x = 0, add_to_list = 0;
    char *mtllib;

    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        //printf("GFMLINE: %s\n", list_at(dat->gfm_lines, i));
    }

    //Remove smoothing groups and get material-library filename
    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        if(strstr(list_at(dat->gfm_lines,i),"mtllib ") != NULL)
        {
            mtllib = list_at(dat->gfm_lines,i);
        }
        if(strstr(list_at(dat->gfm_lines,i),"s ") != NULL)
        {
            list_remove_at(dat->gfm_lines,i);
        }
        if(strstr(list_at(dat->gfm_lines,i),"g default") != NULL)
        {
            list_remove_at(dat->gfm_lines,i);
        }
    }

    //Each group gets its own list now, we need it to build the grouped mesh
    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        if(strstr(list_at(dat->gfm_lines,i),"g ") != NULL)
        {
            list_add(list_to_grouplists, temp_list);
            temp_list = list_new();

            if(add_to_list == 1)
                x++;
            add_to_list = 1;
        }

        if(add_to_list == 1 && temp_list != NULL)
        {
            list_add(temp_list,list_at(dat->gfm_lines,i));
            //printf("Adding: %s to list %d\n",list_at(dat->gfm_lines,i), x);
        }

        if(i == list_count(dat->gfm_lines)-1)
            list_add(list_to_grouplists, temp_list);
    }

    return list_to_grouplists;
}

VEC9_INT *extract_face_indices_from_line(char *l)
{
    VEC9_INT *v = malloc(sizeof(VEC9_INT));

    char *ptr0,*ptr1,*ptr2,*ptr3,*ptr4,*ptr5,*ptr6,*ptr7,*ptr8,*ptr9;
    ptr0 = strtok(l, " ");

    ptr1 = strtok(NULL, "/");
    ptr2 = strtok(NULL, "/");
    ptr3 = strtok(NULL, " ");

    ptr4 = strtok(NULL, "/");
    ptr5 = strtok(NULL, "/");
    ptr6 = strtok(NULL, " ");

    ptr7 = strtok(NULL, "/");
    ptr8 = strtok(NULL, "/");
    ptr9 = strtok(NULL, " ");

    v->a = atoi(ptr1);
    v->b = atoi(ptr2);
    v->c = atoi(ptr3);

    v->d = atoi(ptr4);
    v->e = atoi(ptr5);
    v->f = atoi(ptr6);

    v->g = atoi(ptr7);
    v->h = atoi(ptr8);
    v->i = atoi(ptr9);

    return v;
}

//Takes a single face via lines
OBJ_Face *make_face(_LIST *face_Lines)
{
    int i;
    char *useless, *matname;
    OBJ_Face *face = malloc(sizeof(OBJ_Face));
    face->triangles = list_new();
    for(i = 0; i <list_count(face_Lines);i++)
    {
        if(strstr(list_at(face_Lines, i), "usemtl") != NULL)
        {
            useless = list_at(face_Lines, i);

            useless = strtok(useless, " ");
            matname = strtok(NULL, " ");
            face->material = matname;
        }
        if(strstr(list_at(face_Lines, i), "f ") != NULL)
        {
            VEC9_INT *v = extract_face_indices_from_line(list_at(face_Lines, i));
            list_add(face->triangles, v);
        }
    }
    return face;
}

//take a group via lines
OBJ_Group *make_single_group(_LIST *l)
{
    int i,j;
    char *group;
    OBJ_Group *g = malloc(sizeof(OBJ_Group));
    g->faces = list_new();
    _LIST *list_of_faces = list_new();
    _LIST *face_temp;
    int startReading = 0;
    int counter = 0;

    for(i = 0; i < list_count(l); i++)
    {
        if(strstr(list_at(l,i),"g ") != NULL)
        {
            group = list_at(l,i);
            list_remove_at(l,i);
        }
    }

    for(i = 0; i < list_count(l); i++)
    {
        if( strstr(list_at(l,i),"usemtl ") != NULL)
        {
            printf("\n\n");
            counter++;
            list_add(list_of_faces, face_temp);

            face_temp = list_new();
        }
        //printf("Line %s to face %d\n",list_at(l,i), counter);
        list_add(face_temp,list_at(l,i));
    }
    list_remove_at(list_of_faces,0);
    list_add(list_of_faces, face_temp);

    //printf("\n\n");

    //CHecking if faces were made
    for(i = 0; i < list_count(list_of_faces); i++)
    {
        _LIST *temp_check = list_at(list_of_faces, i);
        int cnt = list_count(temp_check);
        //printf("\n\nCount of Face %d is %d\n", i, cnt);

        //MAKE THE FACE HERE!
        OBJ_Face *f = make_face(temp_check);
        list_add(g->faces, f);

        for(j = 0; j < list_count(temp_check); j++)
        {
            //printf("%s\n", list_at(temp_check,j));
        }
    }
    g->groupname = group;
    return g;
}

void print_group(OBJ_Group *group)
{
    printf("Printing Group: %s\n", group->groupname);
    int i,j;
    for(i = 0; i < list_count(group->faces); i++)
    {
        OBJ_Face *obj_face = list_at(group->faces, i);
        printf("Face:\nMaterial: %s\n", obj_face->material);
        for(j = 0; j < list_count(obj_face->triangles); j++)
        {
            VEC9_INT *d = list_at(obj_face->triangles,j);
            printf("%d %d %d %d %d %d %d %d %d\n", d->a, d->b, d->c,d->d, d->e, d->f,d->g, d->h, d->i);
        }
    }
}

_LIST* make_groups(_LIST *group_lines)
{
    int i,j;
    _LIST *groups = list_new();

    for(i = 0; i < list_count(group_lines); i++)
    {
        _LIST *l_temp = list_at(group_lines,i);
        OBJ_Group *g = make_single_group(l_temp);
        print_group(g);
        list_add(groups, g);
    }
    return groups;
}

void print_list_as_float3(_LIST *l)
{
    int i;
    for(i = 0; i < list_count(l); i++)
    {
        VEC3 *v;
        v = list_at(l, i);
        printf("%f %f %f\n", v->x, v->y,v->z);
    }
}

void print_list_as_float2(_LIST *l)
{
    int i;
    for(i = 0; i < list_count(l); i++)
    {
        VEC2 *v;
        v = list_at(l, i);
        printf("%d: %f %f\n", i, v->x, v->y);
    }
}

void print_obj_model(OBJ_MODEL *model)
{
    printf("PRINTING MODEL:\n");
    print_list_as_float3(model->p);
    print_list_as_float2(model->uv);
    print_list_as_float3(model->n);

    int i;
    for(i = 0; i < list_count(model->groups); i++)
    {
        OBJ_Group *g = list_at(model->groups, i);
        printf("Groupname: %s\n", g->groupname);
        int j;
        for(j = 0; j < list_count(g->faces); j++)
        {
            OBJ_Face *f = list_at(g->faces,j);
            printf("Mat: %s\n",f->material);
            int k;

            for(k = 0; k < list_count(f->triangles); k++)
            {
                VEC9_INT *d = list_at(f->triangles, k);
                printf("%d %d %d %d %d %d %d %d %d\n", d->a, d->b, d->c,d->d, d->e, d->f,d->g, d->h, d->i);
            }
        }
    }
}

_LIST *load_materials(Obj_data_raw *dat)
{
    char *mat_unhandled;
    char *line;
    int i,j, counter = 0;

    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        if(strstr(list_at(dat->gfm_lines, i), "mtllib "))
        {
            mat_unhandled = list_at(dat->gfm_lines, i);
            line = strtok(mat_unhandled, " ");
            line = strtok(NULL, " ");

            printf("%s is matname\n", line);
            break;
        }
    }

    char *buf = load_text(line);
    _LIST *lines = split_buffer(buf, "\n");
    _LIST *list_of_materials = list_new();
    _LIST *mat_lines_temp = list_new();
    _LIST *obj_material_list = list_new();


    for(i = 0; i < list_count(lines); i++)
    {
        //printf("TEST: %s\n", list_at(lines,i));
    }

    for(i = 0; i < list_count(lines); i++)
    {
        if( strstr(list_at(lines,i),"newmtl ") != NULL)
        {
            printf("\n\n");
            counter++;
            list_add(list_of_materials, mat_lines_temp);

            mat_lines_temp = list_new();
        }
        //printf("Line %s to face %d\n",list_at(l,i), counter);
        if( list_at(lines,i) != NULL )
        {
            list_add(mat_lines_temp,list_at(lines,i));
        }
    }
    list_remove_at(list_of_materials,0);
    list_add(list_of_materials, mat_lines_temp);

    printf("Found %d materials\n", list_count(list_of_materials));

    //Test printing
    for(i = 0; i < list_count(list_of_materials) ; i++)
    {
        _LIST *lt = list_at(list_of_materials, i);
        printf("\nPrinting Material %d now:\n", i);
        for(j = 0; j < list_count(lt); j++)
        {
            if(list_at(lt,j) != NULL)
                printf("%s\n", list_at(lt,j));
        }
    }

    //Now extract all the needed lines for our material
    for(i = 0; i < list_count(list_of_materials); i++)
    {
        _LIST *lt = list_at(list_of_materials, i);

        OBJ_MATERIAL *mat = malloc(sizeof(OBJ_MATERIAL));

        for(j = 0; j < list_count(lt); j++)
        {

            char *line = list_at(lt,j);
            if(line == NULL)
                continue;
            if(strstr(line, "newmtl "))
            {
                char *templine = malloc(sizeof(line));
                strcpy(templine, line);
                mat->name = templine;
            }

            if(strstr(line, "illum "))
            {
                char *templine = malloc(strlen(line));
                strcpy(templine, line);

                mat->illum = atof(templine);
            }
            if(strstr(line, "Kd "))
            {

            }
            if(strstr(line, "Ka "))
            {

            }
            if(strstr(line, "Tf "))
            {

            }
            if(strstr(line, "map_Kd "))
            {
                char *templine = malloc(sizeof(line));
                strcpy(templine, line);
                char *useless= strtok(templine," ");
                char *mapkd = strtok(NULL, " ");

                mat->colormap_path = mapkd;
            }
            if(strstr(line, "bump "))
            {
                char *templine = malloc(sizeof(line));
                strcpy(templine, line);
                char *useless= strtok(templine," ");
                char *bump = strtok(NULL, " ");

                mat->normalmap_path = bump;
            }
            if(strstr(line, "Ni "))
            {

            }
            if(strstr(line, "Ks "))
            {

            }
            if(strstr(line, "map_Ks "))
            {
                char *templine = malloc(sizeof(line));
                strcpy(templine, line);
                char *useless= strtok(templine," ");
                char *spec = strtok(NULL, " ");

                mat->specularmap_path = spec;
            }
        }
        list_add(obj_material_list,mat);
    }
    printf("Created: %d OBJ_MATERIALS\n", list_count(obj_material_list));

    return obj_material_list;
}

int main()
{
    char *t = load_text("2g3m.obj");
    _LIST *l = split_buffer(t, "\n");
    Obj_data_raw *dat = extract_points(l);

    _LIST *group_lines = create_group_string(dat);
    _LIST *groups_done = make_groups(group_lines);
    //print_list_as_float3(dat->points);
    //print_list_as_float2(dat->uvs);
    //print_list_as_float3(dat->normals);
    //_LIST *materials = load_materials(dat);

    OBJ_MODEL *m = malloc(sizeof(OBJ_MODEL));
    m->groups = groups_done;
    //m->materials = materials;
    m->n = dat->normals;
    m->p = dat->points;
    m->uv = dat->uvs;

    print_obj_model(m);

    return 0;
}
