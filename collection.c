/*H**********************************************************************
* FILENAME :        collection.c
*
* DESCRIPTION :
*       Collection Types like List, Stack, Queue, KeyValuePair .
* IMPORTANT :
*       All Collection-types only store Pointers to Objects
*       If y
ou need to remove an object, use free on the object,
*       before you remove the entry or clear the list, else you get
*       a memory leak!
*
* PUBLIC FUNCTIONS :
*            _LIST *list_new();
*            int list_clear(_LIST *l);
*            void *list_remove_at(_LIST *l, int index);
*            void list_print(_LIST *l);
*            void *list_add(_LIST *l, void *e);
*            int list_capacity(_LIST *l);
*            int list_count(_LIST *l);
*            int list_empty(_LIST *l);
*            _LIST *list_duplicate(_LIST *l);
*            void **list_toarray(_LIST *l);
*
*            _STACK *stack_new();
*            void stack_push(_STACK *s, void *e);
*            void *stack_pop(_STACK *s);
*            int stack_clear(_STACK *s);
*            void stack_delete(_STACK *s);
*            int stack_capacity(_STACK *s);
*            int stack_count(_STACK *s);
*            int stack_empty(_STACK* s);
*            void **stack_toarray(_STACK *s);
*
* AUTHOR :    Nouri A             START DATE :    10.12.2017
*
*H*/

#include "collection.h"
#define PRINT_LIST_INFO 0

#ifdef linux
    include <stdlib.h>
#endif // linux

#ifdef _WIN32
    #include <stdio.h>
#endif // _WIN32

///******************************LIST*****************************

///Create a new list with standard 16 capacity
_LIST *list_new()
{
    _LIST *l;

    l = malloc(sizeof(_LIST));
    if(l == NULL)
    {
        fprintf(stderr, "Error: Cannt allocate memory for _LIST");
        return NULL;
    }
    l->_capacity = 64;
    l->_count = 0;

    l->_data = (void**)malloc(sizeof(int*)*l->_capacity);

    if(l->_data == NULL)
    {
        fprintf(stderr, "Error: couldn't allocate memory for l->_data");
        free(l);
        return NULL;
    }
    return l;
}

///looses all pointers to the elements and reverts back to
///16 capacity and NULL elements
int list_clear(_LIST *l)
{
    int c;
    if(l == NULL)
    {
        fprintf(stderr, "Error: _LIST pointer is NULL");
        return NULL;
    }

    free(l->_data);
    c = l->_count;
    l->_capacity = 16;
    l->_count = 0;

    l->_data = malloc(sizeof(void*)*l->_capacity);

    if(l->_data == NULL){
        fprintf(stderr, "Error: Couldn't allocate memory for l->_data");
        free(l);
        return NULL;
    }

    if(PRINT_LIST_INFO) printf("List cleared\n");
    return c;
}

///Remove an entry by index, other entries above index will be moved one position down
void *list_remove_at(_LIST *l, int index)
{
    int i;
    void *element;
    if(l != NULL && l->_count > 0)
    {
        element = l->_data[index];
        for(i = index; i < l->_count-1; i++)
        {
            if(i +1 == l->_count)
                break;
            l->_data[i] = l->_data[i+1];
        }
        l->_data[l->_count-1] = NULL;
        l->_count -= 1;

        if(PRINT_LIST_INFO) printf("Item removed at: %d\n", index);

        ///Check count and decrease list size
        ///We assume all pointers above _count to be NULL
        if(l->_count < l->_capacity / 2 && l->_capacity > 16)
        {
            l->_capacity -= 16;

            void **new_list = malloc(sizeof(void*) * l->_capacity);

            for(i = 0; i < l->_count; i++)
            {
                new_list[i] = l->_data[i];
            }

            free(l->_data);
            l->_data = new_list;

            if(PRINT_LIST_INFO) printf("List Capacity decreased to: %d\n", l->_capacity);
        }
        return element;
    }
    else
    {
        return NULL;
    }
}

///Remove an entry by index, other entries above index will be moved one position down
void list_print(_LIST *l)
{
    int i;
    if(l != NULL)
    {
        printf("Printing list:\n");
        for(i = 0; i < l->_count; i++)
        {
            printf("[%d] : %u\n",i,l->_data[i]);
        }
    }
    return;
}

///Add an entry to the list
void *list_add(_LIST *l, void *e)
{
    int i;
    if( l == NULL || e == NULL)
    {
        if(PRINT_LIST_INFO) fprintf(stderr, "Error: _LIST pointer is NULL or Object is NULL");
        return NULL;
    }
    if(l->_count < l->_capacity)
    {
        l->_data[l->_count] = e;
        l->_count++;

        return l->_data[l->_count];
    }
    else ///Increase capacity
    {
        l->_capacity += 64;

        void **data = malloc(sizeof(void*) * l->_capacity);

        if(data  == NULL)
        {
            fprintf(stderr, "Error: Couldn't allocate Memory to increase _LIST");
            return NULL;
        }
        //Copy old list to new list
        for(i =0; i < l->_count; i++)
        {
            data[i] = l->_data[i];
        }

        ///replace with new
        free(l->_data);
        l->_data = data;
        //printf("Increased List");

        ///now append object on the end
        l->_data[l->_count] = e;

        l->_count++;


        return l->_data[l->_count];
    }
}

///Print capacity
int list_capacity(_LIST *l)
{
    if(l != NULL)
        return l->_capacity;
    else
        return -1;
}

///print current count
int list_count(_LIST *l)
{
    if(l != NULL)
        return l->_count;
    else
        return -1;
}

///Check if list is empty
int list_empty(_LIST *l)
{
    if(l != NULL)
        return l->_count < 1 ? 1 : 0;
    else
        return -1;
}

///Make a duplicate of the list
_LIST *list_duplicate(_LIST *l)
{
    _LIST *newl;
    int i;

    if(l == NULL)
        return NULL;

    newl = malloc(sizeof(_LIST));
    if(newl == NULL) { fprintf(stderr,"Error: Failed to alloc list for duplication\n"); return NULL; }
    newl->_count = l->_count;
    newl->_capacity = l->_capacity;
    newl->_data = malloc(sizeof(void*)*newl->_capacity);
    if(newl->_data == NULL) { fprintf(stderr,"Error: Failed to Allocate Data for List for Duplication\n"); return NULL; }

    for(i = 0; i < newl->_count; i++)
        newl->_data[i] = l->_data[i];

    return newl;
}

///copy the list to an array and return the result
void **list_toarray(_LIST *l)
{
    void **arr;
    int i;

    if(l == NULL)
        return NULL;

    arr = malloc(sizeof(void*)*l->_count);
    if(arr == NULL) { fprintf(stderr,"Error: Failed to allocte array for copy-to-array\n"); return NULL; }

    for(i = 0; i < l->_count; i++)
        arr[i] = l->_data[i];

    return arr;
}

///get by index
void *list_at(_LIST *l, int index)
{
    if(l == NULL)
    {
        fprintf(stderr,"can't get item, list is NULL\n");
        return NULL;
    }

    if(index < list_count(l))
    {
        return l->_data[index];
    }
    else
    {
        fprintf(stderr,"Index out of range\n");
        return NULL;
    }

}


///******************************STACK*****************************

///Create a new stack
_STACK *stack_new()
{
    _STACK *s;

    s = malloc(sizeof(_STACK));
    if(s == NULL)
    {
        fprintf(stderr, "Error: Can't allocate memory for _STACK");
        return NULL;
    }
    s->_capacity = 16;
    s->_count = 0;

    s->_data = (void**)malloc(sizeof(int*)*s->_capacity);

    if(s->_data == NULL)
    {
        fprintf(stderr, "Couldn't allocate memory for s->_data");
        free(s);
        return NULL;
    }
    return s;
}

///Put an element on the stack
void stack_push(_STACK *s, void *e)
{
    int i;
    if( s == NULL || e == NULL)
    {
        if(PRINT_LIST_INFO) fprintf(stderr, "Error: _STACK pointer is NULL or Object is NULL");
        return NULL;
    }
    if(s->_count < s->_capacity)
    {
        s->_data[s->_count] = e;
        s->_count++;

        return s->_data[s->_count];
    }
    else ///Increase capacity
    {
        s->_capacity += 16;

        void **data = malloc(sizeof(void*) * s->_capacity);

        if(data  == NULL)
        {
            fprintf(stderr, "Error: Couldn't allocate Memory to increase _STACK");
            return NULL;
        }
        //Copy old list to new list
        for(i =0; i < s->_count; i++)
        {
            data[i] = s->_data[i];
        }
        ///replace with new
        free(s->_data);
        s->_data = data;

        ///now append object on the end
        s->_data[s->_count] = e;
        s->_count++;
    }
}

///Retrieve an element from the stack
void *stack_pop(_STACK *s)
{
    int i;
    void *element;

    if(s != NULL && s->_count > 0)
    {
        element = s->_data[s->_count-1];
        s->_data[s->_count-1] = NULL;
        s->_count--;

        if(s->_count < s->_capacity / 2)
        {
            void **newdat = malloc(sizeof(void*) *s->_capacity);
            if(newdat == NULL)
            {
                fprintf(stderr, "Error: Couldnt allocate memory to decrease stack\n");
                return NULL;
            }
            for(i = 0; i < s->_count;i++)
            {
                newdat[i] = s->_data[i];
            }
            free(s->_data);

            s->_data = newdat;
        }
        return element;
    }
    else
    {
        return NULL;
    }
}

///Reset the stack to standard 16 capacity and clears entries
int stack_clear(_STACK *s)
{
    int i;
    if(s == NULL)
    {
        fprintf(stderr, "Error: Stack cant be cleared, is null n");
        return -1;
    }
    free(s->_data);
    s->_capacity = 16;
    s->_count = 0;
    s->_data = malloc(sizeof(void*)* s->_capacity);
    if (s->_data == NULL)
    {
        fprintf(stderr, "Error: Couldnt allocate memory for data in stack_clear\n n");
        free(s);
        return -1;
    }
    return 0;
}

///Clear all pointers and set  *s to NULL
void stack_delete(_STACK *s)
{
    if(s == NULL)
    {
        fprintf(stderr, "Error: Stack cant be deleted, is null n");
        return NULL;
    }
    free(s->_data);
    free(s);
    return;
}

///Print capacity
int stack_capacity(_STACK *s)
{
    if(s != NULL)
        return s->_capacity;
    else
        return -1;
}

///print current count
int stack_count(_STACK *s)
{
    if(s != NULL)
        return s->_count;
    else
        return -1;
}

///Print the stack
void stack_print(_STACK *s)
{
    int i;
    if(s != NULL)
    {
        printf("Printing list:\n");
        for(i = 0; i < s->_count; i++)
        {
            printf("[%d] : %u\n",i,s->_data[i]);
        }
    }
    return;
}

///Check if stack is empty
int stack_empty(_STACK* s)
{
    if(s != NULL)
        return s->_count < 1 ? 1 : 0;
    else
        return -1;
}

///Returns an array with pointers and size = _count
void **stack_toarray(_STACK *s)
{
    void **arr;
    int i;

    if(s == NULL)
        return NULL;

    arr = malloc(sizeof(void*)*s->_count);
    if(arr == NULL) { fprintf(stderr,"Error: Failed to alloc array for copy-to-array\n"); return NULL; }

    for(i = 0; i < s->_count; i++)
        arr[i] = s->_data[i];

    return arr;
}

