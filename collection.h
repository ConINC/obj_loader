#ifndef COLLECTION_H_
#define COLLECTION_H_

#ifdef linux
    #include <stdlib.h>
#endif

#ifdef _WIN32
    #include <stdio.h>
#endif


typedef struct list_dynamic_str
{
    int _capacity;
    int _count;
    void **_data;
}_LIST;

typedef struct stack_str
{
    int _capacity;
    int _count;
    void **_data;
}_STACK;


/******************************LIST*****************************/
///Create a new list with standard 16 capacity
_LIST *list_new();

///looses all pointers to the elements and reverts back to
///16 capacity and NULL elements
int list_clear(_LIST *l);

///Remove an entry by index, other entries above index will be moved one position down
void *list_remove_at(_LIST *l, int index);

///Remove an entry by index, other entries above index will be moved one position down
void list_print(_LIST *l);

///Add an entry to the list
void *list_add(_LIST *l, void *e);

///Print capacity
int list_capacity(_LIST *l);
///print current count
int list_count(_LIST *l);

///Check if list is empty
int list_empty(_LIST *l);

///Make a duplicate of the list
_LIST *list_duplicate(_LIST *l);

///copy the list to an array and return the result
void **list_toarray(_LIST *l);

///get by index
void *list_at(_LIST *l, int index);

/******************************STACK*****************************/

///Create a new stack
_STACK *stack_new();
///Put an element on the stack
void stack_push(_STACK *s, void *e);
///Retrieve an element from the stack
void *stack_pop(_STACK *s);

///Reset the stack to standard 16 capacity and clears entries
int stack_clear(_STACK *s);
///Clear all pointers and set  *s to NULL
void stack_delete(_STACK *s);

///print stack
void stack_print(_STACK *s);

///Print capacity
int stack_capacity(_STACK *s);
///print current count
int stack_count(_STACK *s);

///Check if stack is empty
int stack_empty(_STACK* s);

///Returns an array with pointers and size = _count
void **stack_toarray(_STACK *s);

/******************************KeyValue*****************************/





#endif // COLLECTION_H_INCLUDED
